﻿using LendFoundry.Configuration.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager.Client;
using LendFoundry.Tenant.Client;
using Moq;
using Xunit;

namespace Docitt.ConsentTracker.Tests
{
    public class ConsentTrackerServiceFactoryTests : InMemoryObjects
    {
        private Mock<IServiceProvider> ServiceProvider { get; } = new Mock<IServiceProvider>();

        [Fact]
        public void Constructor_When_ExecutionOk()
        {
            var serviceFactory = new ConsentTrackerServiceFactory(Mock.Of<System.IServiceProvider>());

            Assert.NotNull(serviceFactory);
        }

        [Fact]
        public void Create_When_ExecutionOk()
        {
            var emptyReader = new StaticTokenReader(string.Empty);
            var tokenReader = It.IsAny<ITokenReader>();

            ServiceProvider.Setup(x => x.GetService(typeof(ITenantService)))
                .Returns(TenantService.Object);

            ServiceProvider.Setup(x => x.GetService(typeof(ITenantTime)))
                .Returns(TenantTime.Object);

            var eventHubServiceFactory = new Mock<IEventHubClientFactory>();
            eventHubServiceFactory.Setup(x => x.Create(tokenReader))
                .Returns(EventHub.Object);

            ServiceProvider.Setup(x => x.GetService(typeof(IEventHubClientFactory)))
                .Returns(eventHubServiceFactory.Object);

            ServiceProvider.Setup(x => x.GetService(typeof(IConfigurationServiceFactory)))
                .Returns(ConfigurationFactory.Object);

            ConfigurationService.Setup(x => x.Get())
                .Returns(Configuration.Object);
            ServiceProvider.Setup(x => x.GetService(typeof(IConfigurationService<ConsentTrackerConfiguration>)))
                .Returns(ConfigurationService.Object);

            var documentGeneratorServiceFactory = new Mock<IDocumentGeneratorServiceFactory>();
            documentGeneratorServiceFactory.Setup(x => x.Create(tokenReader))
                .Returns(DocumentGenerator.Object);

            ServiceProvider.Setup(x => x.GetService(typeof(IDocumentGeneratorServiceFactory)))
                .Returns(documentGeneratorServiceFactory.Object);

            var templateManagerFactory = new Mock<ITemplateManagerServiceFactory>();
            templateManagerFactory.Setup(x => x.Create(tokenReader))
                .Returns(TemplateManager.Object);

            ServiceProvider.Setup(x => x.GetService(typeof(ITemplateManagerServiceFactory)))
                .Returns(templateManagerFactory.Object);

            var documentManagerFactory = new Mock<IDocumentManagerServiceFactory>();
            documentManagerFactory.Setup(x => x.Create(tokenReader))
                .Returns(DocumentManager.Object);

            ServiceProvider.Setup(x => x.GetService(typeof(IDocumentManagerServiceFactory)))
                .Returns(documentManagerFactory.Object);

            ServiceProvider.Setup(x => x.GetService(typeof(ILookupService)))
                .Returns(Lookup.Object);

            var serviceFactory = new ConsentTrackerServiceFactory
                (
                    ServiceProvider.Object as System.IServiceProvider
                );

            Assert.NotNull(serviceFactory);

            var service = serviceFactory.Create(emptyReader, Logger.Object);
            Assert.NotNull(service);
        }
    }

    public interface IServiceProvider : System.IServiceProvider
    {
        T GetService<T>() where T : class;

        T GetRequiredService<T>() where T : class;
    }
}