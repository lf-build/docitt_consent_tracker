﻿using Docitt.ConsentTracker.Configuration;
using LendFoundry.DocumentGenerator;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
using Microsoft.AspNet.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Docitt.ConsentTracker.Tests
{
    public class ServiceTests : InMemoryObjects
    {
        [Fact]
        public async void Acknowledge_When_ExecutionOk()
        {
            // arrange
            TenantService.Setup(x => x.Current).Returns(TenantInfo);
            TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
            EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(true);
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            HttpContextAccessor.Setup(x => x.HttpContext).Returns(Mock.Of<HttpContext>());
            Lookup.SetReturnsDefault(LookupEntries);
            var byteArray
                = new byte[8];
            Random random = new Random();
            random.NextBytes(byteArray);

            DocumentGenerator.Setup(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<IDocument>()))
                .ReturnsAsync(new DocumentResult {Content = byteArray});

            var uploadedDocument = new LendFoundry.DocumentManager.Document
            {
                FileName = "DocumentName"
            };
            DocumentManager
                .Setup(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
                .ReturnsAsync(uploadedDocument);

            // action
            var consentTracker = await Service.Acknowledge("application", "EntityId", "ConsentName", new
            {
                Title = " ######## TITLE ######",
                Body = "###### BODY #####"
            });

            // assert
            Assert.NotNull(consentTracker);
            Assert.Equal(consentTracker.EntityType, "application");

            DocumentGenerator.Verify(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<IDocument>()), Times.AtLeastOnce);

            EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async void Acknowledge_When_HasNoEntityId()
        {
            await Assert.ThrowsAsync<InvalidArgumentException>(async () => 
            {
                // arrange
                TenantService.Setup(x => x.Current).Returns(TenantInfo);
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                HttpContextAccessor.Setup(x => x.HttpContext).Returns(Mock.Of<HttpContext>());
                Lookup.SetReturnsDefault(LookupEntries);
                // action
                var consentTracker = await Service.Acknowledge("entityType", string.Empty, "consent", null);

                // assert
                Assert.Null(consentTracker);

                EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public async void Acknowledge_When_HasNoEntityType()
        {
            await Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                // arrange
                TenantService.Setup(x => x.Current).Returns(TenantInfo);
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                HttpContextAccessor.Setup(x => x.HttpContext).Returns(Mock.Of<HttpContext>());
                Lookup.SetReturnsDefault(LookupEntries);
                // action
                var consentTracker = await Service.Acknowledge(string.Empty, string.Empty, "consent", null);

                // assert
                Assert.Null(consentTracker);

                EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public async void Acknowledge_When_HasNoConsentText()
        {
            await Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                // arrange
                TenantService.Setup(x => x.Current).Returns(TenantInfo);
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                HttpContextAccessor.Setup(x => x.HttpContext).Returns(Mock.Of<HttpContext>());
                Lookup.SetReturnsDefault(LookupEntries);
                // action
                var consentTracker = await Service.Acknowledge("EntityType", "EntityId", string.Empty, null);

                // assert
                Assert.Null(consentTracker);

                EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public async void Acknowledge_When_WasNotFound_Configuration()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                TenantService.Setup(x => x.Current).Returns(TenantInfo);
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                HttpContextAccessor.Setup(x => x.HttpContext).Returns(Mock.Of<HttpContext>());
                Lookup.SetReturnsDefault(LookupEntries);
                Configuration.SetReturnsDefault(InMemoryConfiguration);

                // action
                var service = new ConsentTrackerService
                (
                    Logger.Object,
                    EventHub.Object,
                    TenantService.Object,
                    TenantTime.Object,
                    HttpContextAccessor.Object,
                    Configuration.Object,
                    DocumentGenerator.Object,
                    TemplateManager.Object,
                    DocumentManager.Object,
                    Lookup.Object
                );
                var consentTracker = await service.Acknowledge("Application", "EntityId",  "consent", null);

                // assert
                Assert.Null(consentTracker);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public async void Acknowledge_When_WasNotFound_Entyties()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                TenantService.Setup(x => x.Current).Returns(TenantInfo);
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                HttpContextAccessor.Setup(x => x.HttpContext).Returns(Mock.Of<HttpContext>());

                Configuration.SetReturnsDefault(InMemoryConfiguration);
                Lookup.SetReturnsDefault(LookupEntries);
                // action
                var service = new ConsentTrackerService
                (
                    Logger.Object,
                    EventHub.Object,
                    TenantService.Object,
                    TenantTime.Object,
                    HttpContextAccessor.Object,
                    Configuration.Object,
                    DocumentGenerator.Object,
                    TemplateManager.Object,
                    DocumentManager.Object,
                    Lookup.Object
                );
                var consentTracker = await service.Acknowledge("Application", "EntityId", "consent", null);

                // assert
                Assert.Null(consentTracker);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public async void Acknowledge_When_WasNotFound_Template()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                // arrange
                TenantService.Setup(x => x.Current).Returns(TenantInfo);
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                HttpContextAccessor.Setup(x => x.HttpContext).Returns(Mock.Of<HttpContext>());
                Lookup.SetReturnsDefault(LookupEntries);
                ITemplate template = null;
                TemplateManager.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>()))
                    .ReturnsAsync(template);

                // action
                var consentTracker = await Service.Acknowledge("Application","EntityId", "ConsentName", new { Test = "Test" });

                // assert
                Assert.Null(consentTracker);

                EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.Never);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            });
        }


        [Fact]
        public async void View_When_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Lookup.SetReturnsDefault(LookupEntries);
            var template = new Template
            {
                Body = "Body",
                Format = Format.Html,
                Name = "TemplateName",
                Properties = new Dictionary<string, object>(),
                Version = "TemplateVersion"
            };
            TemplateManager.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>()))
                .ReturnsAsync(template);

            var templateResult = new LendFoundry.TemplateManager.TemplateResult { Data = "string related to TemplateManager result" };
            TemplateManager.Setup(x => x.Process(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<object>()))
                .ReturnsAsync(templateResult);

            // action
            var consentTrackerTemplateResult = await Service.View("Application", "ConsentName", new { Test = "Test" });

            // assert
            Assert.NotNull(consentTrackerTemplateResult);
            Assert.NotNull(consentTrackerTemplateResult.Data);
            Assert.Equal(templateResult.Data, consentTrackerTemplateResult.Data);

            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            TemplateManager.Verify(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>()), Times.AtLeastOnce);
            TemplateManager.Verify(x => x.Process(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<object>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void View_When_HasNo_EntityType()
        {
            await Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Lookup.SetReturnsDefault(LookupEntries);
                // action
                var consentTrackerTemplateResult = await Service.View(string.Empty, "consent", null);

                // assert
                Assert.Null(consentTrackerTemplateResult);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public async void View_When_HasNo_ConsentName()
        {
            await Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                Lookup.SetReturnsDefault(LookupEntries);
                // action
                var consentTrackerTemplateResult = await Service.View("Application", string.Empty, null);

                // assert
                Assert.Null(consentTrackerTemplateResult);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public async void View_When_WasNotFound_Configuration()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));
                ConsentTrackerConfiguration config = null;
                ConfigurationService.Setup(x => x.Get())
                    .Returns(config);
                Lookup.SetReturnsDefault(LookupEntries);
                // action
                var consentTrackerTemplateResult = await Service.View("Application", "consent", null);

                // assert
                Assert.Null(consentTrackerTemplateResult);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
                Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
                ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            });
        }

        [Fact]
        public async void View_When_WasNotFound_Entyties()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () => 
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Lookup.SetReturnsDefault(LookupEntries);
                InMemoryConfiguration.Entities = null;
                ConfigurationService.Setup(x => x.Get())
                    .Returns(InMemoryConfiguration);

                // action
                var consentTrackerTemplateResult = await Service.View("Application", "ConsentName", new { Test = "Test" });

                // assert
                Assert.Null(consentTrackerTemplateResult);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);

                ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            });
        }

        [Fact]
        public async void View_When_WasNotFound_Schema()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Lookup.SetReturnsDefault(LookupEntries);
                InMemoryConfiguration.Entities[0].Consents = null;
                ConfigurationService.Setup(x => x.Get())
                    .Returns(InMemoryConfiguration);

                // action
                var consentTrackerTemplateResult = await Service.View("Application", "ConsentName", new { Test = "Test" });

                // assert
                Assert.Null(consentTrackerTemplateResult);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);

                ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            });
        }

        [Fact]
        public async void View_When_WasNotFound_Template()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () => 
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Lookup.SetReturnsDefault(LookupEntries);
                ConfigurationService.Setup(x => x.Get())
                    .Returns(InMemoryConfiguration);

                ITemplate template = null;
                TemplateManager.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>()))
                    .ReturnsAsync(template);

                // action
                var consentTrackerTemplateResult = await Service.View("Application", "ConsentName", new { Test = "Test" });

                // assert
                Assert.Null(consentTrackerTemplateResult);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);

                ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
                TemplateManager.Verify(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>()), Times.AtLeastOnce);
            });
        }

        [Fact]
        public async void View_When_WasNotFound_TemplateResult_AfterProcess()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Lookup.SetReturnsDefault(LookupEntries);
                ConfigurationService.Setup(x => x.Get())
                    .Returns(InMemoryConfiguration);

                ITemplate template = null;
                TemplateManager.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>()))
                    .ReturnsAsync(template);

                LendFoundry.TemplateManager.ITemplateResult templateResult = null;
                TemplateManager.Setup(x => x.Process(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<object>()))
                    .ReturnsAsync(templateResult);

                // action
                var consentTrackerTemplateResult = await Service.View("Application", "ConsentName", new { Test = "Test" });

                // assert
                Assert.Null(consentTrackerTemplateResult);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);

                ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
                TemplateManager.Verify(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>()), Times.AtLeastOnce);
                TemplateManager.Verify(x => x.Process(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<object>()), Times.AtLeastOnce);
            });
        }
    }
}