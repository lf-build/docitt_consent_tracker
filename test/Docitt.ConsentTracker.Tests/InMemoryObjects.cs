﻿using LendFoundry.Configuration.Client;
using Docitt.ConsentTracker.Configuration;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Http;
using Moq;
using System.Collections.Generic;
using LendFoundry.DocumentGenerator;
using LendFoundry.Foundation.Lookup;

namespace Docitt.ConsentTracker.Tests
{
    public class InMemoryObjects
    {
        protected TenantInfo TenantInfo = new TenantInfo { Id = "my-tenant" };

        protected Mock<IEventHubClient> EventHub { get; } = new Mock<IEventHubClient>();

        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();

        protected Mock<ITenantService> TenantService { get; } = new Mock<ITenantService>();

        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        protected Mock<IHttpContextAccessor> HttpContextAccessor { get; } = new Mock<IHttpContextAccessor>();

        protected Mock<IConfigurationService<ConsentTrackerConfiguration>> ConfigurationService { get; } = new Mock<IConfigurationService<ConsentTrackerConfiguration>>();

        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();

        protected Mock<ConsentTrackerConfiguration> Configuration { get; } = new Mock<ConsentTrackerConfiguration>();

        protected Mock<IDocumentGeneratorService> DocumentGenerator { get; } = new Mock<IDocumentGeneratorService>();

        protected Mock<ITemplateManagerService> TemplateManager { get; } = new Mock<ITemplateManagerService>();

        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        protected Mock<IDocumentManagerService> DocumentManager { get; } = new Mock<IDocumentManagerService>();

        protected Mock<ILookupService> Lookup { get; } = new Mock<ILookupService>();
        protected IConsentTrackerService Service => new ConsentTrackerService
        (
            Logger.Object, 
            EventHub.Object, 
            TenantService.Object, 
            TenantTime.Object, 
            HttpContextAccessor.Object,
            InMemoryConfiguration,
            DocumentGenerator.Object,
            TemplateManager.Object,
            DocumentManager.Object,
            Lookup.Object
        );
        protected Dictionary<string, string> LookupEntries { get; } = new Dictionary<string, string> { { "application", "application" } };
        protected ConsentTrackerConfiguration InMemoryConfiguration { get; } = new ConsentTrackerConfiguration
        {
            Entities = new List<ConsentTrackerEntity>
            {
                new ConsentTrackerEntity
                {
                    EntityName = "Application",
                    Consents = new List<ConsentTrackerConsent>
                    {
                        new ConsentTrackerConsent
                        {
                            Name = "ConsentName",
                            Title = "ConsentTitle",
                            TemplateName = "TemplateName",
                            TemplateVersion = "TemplateVersion",
                            Document = new ConsentTrackerDocument
                            {
                                 Name = "DocumentName",
                                 Tags = new List<string> { "terms", "consents" }.ToArray()
                            }
                        }
                    }.ToArray()
                }
            }.ToArray()
        };
    }
  
}