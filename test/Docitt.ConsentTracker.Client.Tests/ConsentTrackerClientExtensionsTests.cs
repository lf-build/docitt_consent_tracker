﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Docitt.ConsentTracker.Client.Tests
{
    public class ConsentTrackerClientExtensionsTests : InMemoryObjects
    {
        protected Mock<Microsoft.AspNet.Http.IHttpContextAccessor> HttpContextAccessor { get; set; } = new Mock<Microsoft.AspNet.Http.IHttpContextAccessor>();

        protected IServiceCollection ServiceCollection { get; set; } = new ServiceCollection();

        protected Mock<ITokenReader> TokenReader { get; set; } = new Mock<ITokenReader>();

        protected Mock<ILogger> Logger { get; set; } = new Mock<ILogger>();

        protected string Endpoint { get; } = "10.1.1.99";

        protected int Port { get; } = 5000;

        public ConsentTrackerClientExtensionsTests()
        {
            HttpContextAccessor = new Mock<Microsoft.AspNet.Http.IHttpContextAccessor>();
            Logger = new Mock<ILogger>();
            TokenReader = new Mock<ITokenReader>();

            ServiceCollection = new ServiceCollection();

            ServiceCollection.AddSingleton(x => HttpContextAccessor.Object);
            ServiceCollection.AddSingleton(x => Logger.Object);
            ServiceCollection.AddSingleton(x => TokenReader.Object);

            ServiceProvider = ServiceCollection.BuildServiceProvider();
        }

        private System.IServiceProvider ServiceProvider { get; }

        [Fact]
        public void Should_Be_Able_To_AddConsentTrackerService()
        {
            var serviceCollection = ConsentTrackerClientExtensions.AddConsentTrackerService
            (
                ServiceCollection,
                Endpoint,
                Port
            );

            Assert.NotNull(serviceCollection);
            Assert.NotEmpty(serviceCollection);
            Assert.Equal(5, serviceCollection.Count);
            Assert.Equal(1, serviceCollection.Count(x => x.ServiceType == typeof(IConsentTrackerClientFactory)));
            Assert.Equal(1, serviceCollection.Count(x => x.ServiceType == typeof(IConsentTrackerService)));
        }
    }
}