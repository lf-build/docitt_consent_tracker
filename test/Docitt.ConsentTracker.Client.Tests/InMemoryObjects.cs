﻿using System;

namespace Docitt.ConsentTracker.Client.Tests
{
    public class InMemoryObjects
    {
        protected IConsentEntry InMemoryConsentEntry { get; } = new ConsentEntry
        {
            Data = new
            {
                Title = " ######## TITLE ######",
                Body = "###### BODY #####"
            },
            ConsentName = "ConsentName",
            EntityId = "EntityId",
            EntityType = "Application",
            Ip = "192.168.1.47",
            Time = new LendFoundry.Foundation.Date.TimeBucket(new DateTimeOffset(DateTime.Now)),
            Id = Guid.NewGuid().ToString("N")
        };
    }
}