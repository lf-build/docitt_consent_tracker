﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
using Moq;
using Xunit;

namespace Docitt.ConsentTracker.Client.Tests
{
    public class ConsentTrackerClientFactoryTests :InMemoryObjects
    {
        protected Mock<Microsoft.AspNet.Http.IHttpContextAccessor> HttpContextAccessor { get; set; } = new Mock<Microsoft.AspNet.Http.IHttpContextAccessor>();

        protected IServiceCollection ServiceCollection { get; set; } = new ServiceCollection();

        protected Mock<ITokenReader> TokenReader { get; set; } = new Mock<ITokenReader>();

        protected Mock<ILogger> Logger { get; set; } = new Mock<ILogger>();

        protected string Endpoint { get; } = "10.1.1.99";

        protected int Port { get; } = 5000;

        public ConsentTrackerClientFactoryTests()
        {
            HttpContextAccessor = new Mock<IHttpContextAccessor>();
            Logger = new Mock<ILogger>();
            TokenReader = new Mock<ITokenReader>();

            ServiceCollection = new ServiceCollection();

            ServiceCollection.AddSingleton(x => HttpContextAccessor.Object);
            ServiceCollection.AddSingleton(x => Logger.Object);
            ServiceCollection.AddSingleton(x => TokenReader.Object);

            ServiceProvider = ServiceCollection.BuildServiceProvider();
        }

        private System.IServiceProvider ServiceProvider { get; }

        [Fact]
        public void Should_Be_Able_To_Create_ConsentTrackerService()
        {
            var factory = new ConsentTrackerClientFactory(ServiceProvider, Endpoint, Port);
            var client = factory.Create(TokenReader.Object);

            Assert.NotNull(client);
            Assert.IsType<ConsentTrackerServiceClient >(client);
        }
    }
}