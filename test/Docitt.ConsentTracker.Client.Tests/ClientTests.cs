﻿using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using Xunit;

namespace Docitt.ConsentTracker.Client.Tests
{
    public class ClientTests : InMemoryObjects
    {
        private Mock<IServiceClient> Client { get; } = new Mock<IServiceClient>();

        private IConsentTrackerService Service { get; }

        public ClientTests()
        {
            Service = new ConsentTrackerServiceClient(Client.Object);
        }

        [Fact]
        public async void Acknowledge_When_ExecutionOk()
        {
            // arrange
            Client.Setup(c => c.ExecuteAsync<ConsentEntry>(It.IsAny<IRestRequest>()))
                .ReturnsAsync(InMemoryConsentEntry as ConsentEntry);

            // action
            var result = await Service.Acknowledge("EntityType", "EntityId", "ConsentName", new
            {
                Title = " ######## TITLE ######",
                Body = "###### BODY #####"
            });

            // assert
            Assert.NotNull(result);
        }

        [Fact]
        public async void View_When_ExecutionOk()
        {
            // arrange
            Client.Setup(c => c.ExecuteAsync<TemplateResult>(It.IsAny<IRestRequest>()))
                .ReturnsAsync(new TemplateResult { Data = "<div class=entry><h1>Testing pdf output</h1><h2>{{title}}</h2><div class=body>{{body}}</div></div>" });

            // action
            var result = await Service.View("EntityType", "ConsentName", new
            {
                Title = " ######## TITLE ######",
                Body = "###### BODY #####"
            });

            // assert
            Assert.NotNull(result);
            Assert.NotNull(result.Data);
        }
    }
}