﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;

namespace Docitt.ConsentTracker.Api.Controllers
{
    /// <summary>
    /// API Controller
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public ApiController(IConsentTrackerService service, ILogger logger) : base(logger)
        {
            Service = service;
        }

        private IConsentTrackerService Service { get; }

        /// <summary>
        /// Views the consent.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="consentName">Name of the consent.</param>
        /// <param name="payload">The payload.</param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{consentName}/view")]
#if DOTNET2
        [ProducesResponseType(typeof(ITemplateResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> ViewConsent(string entityType, string consentName, [FromBody]object payload)
        {
            return await ExecuteAsync(async () =>
            {
                if (payload == null)
                    throw new InvalidArgumentException("The payload cannot be null", "payload");

                var templateResult = await Service.View(entityType, consentName, payload);
                return Ok(templateResult);
            });
        }

        /// <summary>
        /// Acknowledges the consent.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="consentName">Name of the consent.</param>
        /// <param name="payload">The payload.</param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/{consentName}/ack")]
#if DOTNET2
        [ProducesResponseType(typeof(IConsentEntry), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> AcknowledgeConsent(string entityType, string entityId, string consentName, [FromBody]object payload)
        {
            return await ExecuteAsync(async () =>
            {
                if (payload == null)
                    throw new InvalidArgumentException("The payload cannot be null", "payload");

                var consentTracker = await Service.Acknowledge(entityType, entityId, consentName, payload);
                return Ok(consentTracker);
            });
        }
    }
}