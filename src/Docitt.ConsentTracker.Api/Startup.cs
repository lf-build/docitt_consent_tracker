﻿using System;
using System.Runtime;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager.Client;
using LendFoundry.Tenant.Client;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.Foundation.ServiceDependencyResolver;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif


namespace Docitt.ConsentTracker.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "ConsentTracker"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
               {
                   Type = "apiKey",
                   Name = "Authorization",
                   Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                   In = "header"
               });
               c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.ConsentTracker.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddConfigurationService<ConsentTrackerConfiguration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();           
            services.AddLookupService();
            services.AddDocumentGenerator();            
            services.AddDocumentManager();    
            services.AddTemplateManagerService();
#if DOTNET2
            services.AddDependencyServiceUriResolver<ConsentTrackerConfiguration>(Settings.ServiceName);
#endif
            // interface implements
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            services.AddTransient<IConsentTrackerServiceFactory, ConsentTrackerServiceFactory>();
            services.AddTransient<IConsentTrackerService, ConsentTrackerService>();
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IConfigurationServiceFactory<ConsentTrackerConfiguration>>().Create(staticTokenReader).Get();
            });
            
            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
            Console.WriteLine(GCSettings.LatencyMode);
            Console.WriteLine(GCSettings.IsServerGC);
            Console.WriteLine(GCSettings.LargeObjectHeapCompactionMode);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            #if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "ConsentTracker Service");
            });
#else
            //app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
            HttpHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());
        }
    }
}