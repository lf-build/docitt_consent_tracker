﻿using LendFoundry.Security.Tokens;

namespace Docitt.ConsentTracker.Client
{
    public interface IConsentTrackerClientFactory
    {
        IConsentTrackerService Create(ITokenReader reader);
    }
}