﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace Docitt.ConsentTracker.Client
{
    public static class ConsentTrackerClientExtensions
    {
        public static IServiceCollection AddConsentTrackerService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddSingleton<IConsentTrackerClientFactory>(p => new ConsentTrackerClientFactory(p, endpoint, port));
            services.AddSingleton(p => p.GetService<IConsentTrackerClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }
    }
}