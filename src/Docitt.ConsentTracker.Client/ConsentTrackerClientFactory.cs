﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace Docitt.ConsentTracker.Client
{
    public class ConsentTrackerClientFactory : IConsentTrackerClientFactory
    {
        public ConsentTrackerClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; set; }

        private string Endpoint { get; set; }

        private int Port { get; set; }

        public IConsentTrackerService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ConsentTrackerServiceClient(client);
        }
    }
}