﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;

namespace Docitt.ConsentTracker.Client
{
    public class ConsentTrackerServiceClient : IConsentTrackerService
    {
        public ConsentTrackerServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IConsentEntry> Acknowledge(string entityType, string entityId, string consentName, object data)
        {
            var requestData = new ConsentEntry
            {
                EntityId = entityId,
                EntityType = entityType,
                ConsentName = consentName,
                Data = data
            };

            return await Client.PostAsync<ConsentEntry,ConsentEntry>($"/{entityType}/{entityId}/{consentName}/ack", requestData, true);
        }

        public async Task<ITemplateResult> View(string entityType, string consentName, object data)
        {           
            var requestData = new ConsentEntry
            {
                ConsentName = consentName,
                EntityType = entityType,
                Data = data
            };

            return await Client.PostAsync<ConsentEntry,TemplateResult>($"/{entityType}/{consentName}/view", requestData, true);
        }
    }
}