﻿using System.Collections.Generic;
using Docitt.ConsentTracker.Configuration;
using LendFoundry.Foundation.Client;

namespace Docitt.ConsentTracker
{
    public class ConsentTrackerConfiguration: IDependencyConfiguration
    {
        public ConsentTrackerEntity[] Entities { get; set; }
        
        public Dictionary<string,string> Dependencies { get; set; }

        public string Database { get; set; }

        public string ConnectionString { get; set; }
    }
}