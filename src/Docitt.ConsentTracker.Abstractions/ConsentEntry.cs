﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace Docitt.ConsentTracker
{
    public class ConsentEntry : Aggregate, IConsentEntry
    {
        public string ConsentName { get; set; }

        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public string Ip { get; set; }

        public object Data { get; set; }

        public TimeBucket Time { get; set; }
    }
}