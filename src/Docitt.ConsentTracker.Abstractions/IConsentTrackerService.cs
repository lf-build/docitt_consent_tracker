﻿using System.Threading.Tasks;

namespace Docitt.ConsentTracker
{
    public interface IConsentTrackerService
    {
        Task<IConsentEntry> Acknowledge(string entityType, string entityId, string consentName, object data);

        Task<ITemplateResult> View(string entityType, string consentName, object data);
    }
}