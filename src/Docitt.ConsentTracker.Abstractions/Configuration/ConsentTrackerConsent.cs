﻿namespace Docitt.ConsentTracker.Configuration
{
    public class ConsentTrackerConsent
    {
        public string Name { get; set; }

        public string Title { get; set; }

        public string TemplateName { get; set; }

        public string TemplateVersion { get; set; }

        public ConsentTrackerDocument Document { get; set; }
    }
}