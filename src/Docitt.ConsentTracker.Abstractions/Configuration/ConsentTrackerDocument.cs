﻿namespace Docitt.ConsentTracker.Configuration
{
    public class ConsentTrackerDocument
    {
        public string Name { get; set; }

        public string[] Tags { get; set; }
    }
}