﻿namespace Docitt.ConsentTracker.Configuration
{
    public class ConsentTrackerEntity
    {
        public string EntityName { get; set; }

        public ConsentTrackerConsent[] Consents { get; set; }
    }
}