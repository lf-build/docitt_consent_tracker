﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.ConsentTracker
{
    public interface IConsentTrackerServiceFactory
    {
        IConsentTrackerService Create(StaticTokenReader reader, ILogger logger);
    }
}