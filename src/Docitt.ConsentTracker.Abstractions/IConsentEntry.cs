﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace Docitt.ConsentTracker
{
    public interface IConsentEntry : IAggregate
    {
        string ConsentName { get; set; }

        string EntityId { get; set; }

        string EntityType { get; set; }

        string Ip { get; set; }

        object Data { get; set; }

        TimeBucket Time { get; set; }
    }
}