﻿namespace Docitt.ConsentTracker
{
    public interface ITemplateResult
    {
        string Data { get; set; }
    }
}