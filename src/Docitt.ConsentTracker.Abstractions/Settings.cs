﻿using System;

namespace Docitt.ConsentTracker
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "consent-tracker";
    }
}