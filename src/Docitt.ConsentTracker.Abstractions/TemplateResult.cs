﻿namespace Docitt.ConsentTracker
{
    public class TemplateResult : ITemplateResult
    {
        public string Data { get; set; }
    }
}