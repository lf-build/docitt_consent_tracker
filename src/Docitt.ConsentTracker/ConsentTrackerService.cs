﻿using Docitt.ConsentTracker.Configuration;
using LendFoundry.DocumentGenerator;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
using LendFoundry.Tenant.Client;

#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
using LendFoundry.EventHub;
#endif
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace Docitt.ConsentTracker
{
    public class ConsentTrackerService : IConsentTrackerService
    {
        public ConsentTrackerService
        (
            ILogger logger,
            IEventHubClient eventHubClient,
            ITenantService tenantService,
            ITenantTime tenantTime,
            IHttpContextAccessor httpAccessor,
            ConsentTrackerConfiguration configuration,
            IDocumentGeneratorService documentGeneratorService,
            ITemplateManagerService templateManagerService,
            IDocumentManagerService documentManagerService,
            ILookupService lookup
        )
        {
            Logger = logger;
            EventHub = eventHubClient;
            TenantService = tenantService;
            TenantTime = tenantTime;
            HttpAccessor = httpAccessor;
            if (configuration == null)
                throw new ArgumentNullException("The Configuration related to the Consent Tracker cannot be null");
            Configuration = configuration;
            DocumentGenerator = documentGeneratorService;
            TemplateManager = templateManagerService;
            DocumentManager = documentManagerService;
            Lookup = lookup;
        }
        private ILogger Logger { get; }
        private IEventHubClient EventHub { get; }
        private ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }
        private IHttpContextAccessor HttpAccessor { get; }
        private ConsentTrackerConfiguration Configuration { get; }
        private IDocumentGeneratorService DocumentGenerator { get; }
        private ITemplateManagerService TemplateManager { get; }
        private IDocumentManagerService DocumentManager { get; }
        private ILookupService Lookup { get; }
        public async Task<IConsentEntry> Acknowledge(string entityType, string entityId, string consentName, object data)
        {
            try
            {
                entityType = Validate(entityType, consentName);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new InvalidArgumentException($"The {nameof(entityId)} cannot be null", nameof(entityId));
                var consentSchema = GetConsentByConfiguration(entityType, consentName);
                if (consentSchema == null)
                    throw new NotFoundException($"The consent:{consentName} was not found");
                if (consentSchema.Document == null)
                    throw new NotFoundException("The Document was not found in the configuration");

                var tenantId = TenantService.Current.Id;
                var currentTime = TenantTime.Now;
                var eventName = UppercaseFirst($"{entityType}ConsentAccepted");
                var consent = new ConsentEntry
                {
                    Id = Guid.NewGuid().ToString("N"),
                    TenantId = tenantId,
                    EntityType = entityType,
                    EntityId = entityId,
                    Ip = GetClientIp(),
                    ConsentName = consentSchema.Name,
                    Time = new TimeBucket(currentTime),
                    Data = data
                };
                object metaData = null;
                dynamic documentData = JObject.FromObject(data);
               
                if (((JObject)documentData).Properties().Any(p => p.Name == "Data"))
                    documentData.Data.IpAddress = consent.Ip;
                if (((JObject)documentData).Properties().Any(p => p.Name == "metaData"))
                    metaData = documentData.metaData;

                documentData.IpAddress = consent.Ip;
                documentData.CurrentDate = consent.Time.Time.ToString("MM/dd/yyyy hh:mm:ss tt");
                documentData.currentDate = documentData.CurrentDate;

                var document = new LendFoundry.DocumentGenerator.Document
                {
                    Data = documentData,
                    Format = DocumentFormat.Pdf,
                    Name = consentSchema.Document.Name,
                    Version = consentSchema.TemplateVersion
                };


                var documentResult = await DocumentGenerator.Generate(consentSchema.TemplateName, consentSchema.TemplateVersion, Format.Html, document);
                if (documentResult == null || documentResult.Content.Length == 0)
                    throw new NotFoundException($"The Document with template name:{consentSchema.TemplateName} was not found");

                using (var stream = new MemoryStream(documentResult.Content))
                {
                    var uploadedDocument = await DocumentManager.Create
                        (
                            stream,
                            entityType,
                            entityId,
                            documentResult.DocumentName,
                            metaData,
                            consentSchema.Document.Tags.ToList()
                        );
                    if (uploadedDocument == null)
                        throw new NotFoundException("The Document cannot be uploaded");
                }

                await EventHub.Publish(eventName, consent);

                Logger.Info($"The {eventName} was published with object:{consent}");
                return consent;
            }
            catch (Exception ex)
            {
                Logger.Error($"The Acknowledge({entityType}, {entityId}, {consentName}) method raised an error: {ex.Message}");
                throw;
            }
        }

        public async Task<ITemplateResult> View(string entityType, string consentName, object data)
        {
            try
            {
                entityType = Validate(entityType, consentName);
                var consent = GetConsentByConfiguration(entityType, consentName);
                if (consent == null)
                    throw new NotFoundException($"The consent:{consentName} was not found");

                var template = await TemplateManager.Get(consent.TemplateName, consent.TemplateVersion, Format.Html);
                if (template == null)
                    throw new NotFoundException($"The TemplateName:{consent.TemplateName} was not found");

                var templateResult = await TemplateManager.Process(template.Name, template.Version, template.Format, data);
                if (templateResult == null)
                    throw new NotFoundException($"The TemplateName:{consent.TemplateName} was not found or cannot be processed");

                return new TemplateResult { Data = templateResult.Data };
            }
            catch (Exception ex)
            {
                Logger.Error($"The View({entityType}, {consentName}, {data}) method raised an error:{ex.Message}");
                throw;
            }
        }

        private ConsentTrackerConsent GetConsentByConfiguration(string entityType, string consentName)
        {
            if (Configuration.Entities == null)
                throw new NotFoundException($"The entity:{entityType} was not found");

            var entity = Configuration.Entities.FirstOrDefault(e => e.EntityName.Equals(entityType, StringComparison.OrdinalIgnoreCase));

            if (entity == null)
                throw new NotFoundException($"The entity:{entityType} was not found");

            if (entity.Consents == null)
                throw new NotFoundException($"The entity:{entityType} does not have schemas");

            return entity.Consents.FirstOrDefault(c => c.Name.Equals(consentName, StringComparison.OrdinalIgnoreCase));
        }

        private string Validate(string entityType, string consentName)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException($"The {nameof(entityType)} cannot be null", nameof(entityType));
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(consentName))
                throw new InvalidArgumentException($"The {nameof(consentName)} cannot be null", nameof(consentName));
            return entityType;
        }

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();

            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);
            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

        private string GetClientIp()
        {
            const string message = "Cannot determine client IP";
            try
            {
                if (HttpAccessor == null)
                    throw new ArgumentNullException(message);
                if (HttpAccessor.HttpContext == null)
                    throw new ArgumentNullException(message);
                if (HttpAccessor.HttpContext.Request == null)
                    throw new ArgumentNullException(message);
                if (!HttpAccessor.HttpContext.Request.Headers.Any(h => h.Key.ToLower() == "x-client-ip"))
                    throw new ArgumentNullException(message);
                return HttpAccessor.HttpContext.Request.Headers["X-Client-IP"];
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetClientId('{message}') raised an error\n: {ex.Message}");
            }
            return null;
        }
        private static string UppercaseFirst(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }
            return $"{char.ToUpper(text[0])}{text.Substring(1)}";
        }
    }
}