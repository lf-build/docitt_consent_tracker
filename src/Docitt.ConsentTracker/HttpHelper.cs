﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
namespace Docitt.ConsentTracker
{
    public static class HttpHelper
    {
        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }

        private static IHttpContextAccessor HttpContextAccessor;

        public static IHttpContextAccessor HttpContextAcessor
        {
            get
            {
                return HttpContextAccessor;
            }
        }
    }
}