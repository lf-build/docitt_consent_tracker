﻿using LendFoundry.Configuration;
using IDocumentGeneratorServiceFactory = LendFoundry.DocumentGenerator.Client.IDocumentGeneratorServiceFactory;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager.Client;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
using LendFoundry.EventHub;
#endif
using System;

using LendFoundry.Foundation.Lookup;

namespace Docitt.ConsentTracker
{
    public class ConsentTrackerServiceFactory : IConsentTrackerServiceFactory
    {
        public ConsentTrackerServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IConsentTrackerService Create(StaticTokenReader reader, ILogger logger)
        {
            var tenantService = Provider.GetService<ITenantService>();

            var tenantTime = Provider.GetService<ITenantTime>();

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var httpAccessor = HttpHelper.HttpContextAcessor;

            var configurationFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configuration = Provider.GetService<IConfigurationService<ConsentTrackerConfiguration>>().Get();

            var documentGeneratorFactory = Provider.GetService<IDocumentGeneratorServiceFactory>();
            var documentGenerator = documentGeneratorFactory.Create(reader);

            var templateManagerFactory = Provider.GetService<ITemplateManagerServiceFactory>();
            var templateManager = templateManagerFactory.Create(reader);

            var documentManagerFactory = Provider.GetService<IDocumentManagerServiceFactory>();
            var documentManager = documentManagerFactory.Create(reader);

            var lookup = Provider.GetService<ILookupService>();
           
            return new ConsentTrackerService(logger, eventHub, tenantService, tenantTime, httpAccessor, configuration, documentGenerator, templateManager, documentManager, lookup);
        }
    }
}